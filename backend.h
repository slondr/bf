#pragma once
#ifndef __BACKEND_H_
#define __BACKEND_H_

/* Backend function headers for Brainfuck commands */

// >
void inc_p(char * page, char * ptr);

// <
void dec_p(char * page, char * ptr);

// +
void inc_d(char * page, char * ptr);

// -
void dec_d(char * page, char * ptr);

// .
void out_d(char * page, char * ptr);

// ,
void in_d (char * page, char * ptr);

// [
void jz(char * page, char * ptr, char * rsp);

// ]
char * loop(char * page, char * ptr);

#endif
