#include <stdio.h>
#include <stdlib.h>

#include "backend.h"

void interp_program(char * page, char * ptr, char * program) {
  while(*program) {
    char * jmp_val;
    switch(*program) {
    case '>':
      inc_p(page, ptr);
      break;
    case '<':
      dec_p(page, ptr);
      break;
    case '+':
      inc_d(page, ptr);
      break;
    case '-':
      dec_d(page, ptr);
      break;
    case '.':
      out_d(page, ptr);
      break;
    case ',':
      in_d(page, ptr);
      break;
    case '[':
      jz(page, ptr, program);
      break;
    case ']':
      jmp_val = loop(page, ptr);
      if(jmp_val)
	program = --jmp_val;
      break;
    }
    ++program;
  }
}

int main(int argc, char ** argv) {
  if(argc != 2) {
    fprintf(stderr, "Incorrect number of arguments\n");
    return 1;
  }

  printf("bf [%s] v1.0\n", argv[0]);

  char * tape = (char *) calloc(30000, 1);
  char * data_pointer = tape;

  interp_program(tape, data_pointer, argv[1]);

  return 0;
}
