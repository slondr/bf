/// Backend function implementations
#include <stdlib.h>
#include <stdio.h>

// global stack to be used as an address pointer return stack
char * stack[1000];
char ** sp = stack;

#define push(n) (*((sp)++) = (n))
#define pop (*--(sp))

// Incremented if we pass over a block
char head_up = 0;

// Implentation of brainfuck functions

// >
void inc_p(char * page, char * ptr) {
  if(!head_up) {
    ++ptr;
  }
}

void dec_p(char * page, char * ptr) {
  if(!head_up) {
    --ptr;
  }
}

void inc_d(char * page, char * ptr) {
  if(!head_up) {
    ++(*ptr);
  }
}

void dec_d(char * page, char * ptr) {
  if(!head_up) {
    --(*ptr);
  }
}

void out_d(char * page, char * ptr) {
  if(!head_up) {
    putchar(*ptr);
  }
}

void in_d(char * page, char * ptr) {
  if(!head_up) {
    (*ptr) = getchar();
  }
}

void jz(char * page, char * ptr, char * rsp) {
  if(!head_up) {
    push(rsp);
  } else {
    ++head_up;
  }
}

char * loop(char * page, char * ptr) {
  if(!head_up) {
    return pop;
  } else {
    --head_up;
    return 0;
  }
}
